= Test

```bash
python -m pytest tests/ -v                                                  (env: sam-python-test)──(2018-12-18 13:45)─┘
========================================================= test session starts ==========================================================
platform darwin -- Python 3.7.1, pytest-4.0.2, py-1.7.0, pluggy-0.8.0 -- /Users/mvanbaak/.dotfiles/virtualenvs/sam-python-test/bin/python
cachedir: .pytest_cache
rootdir: /Users/mvanbaak/dev/frontrow/sam-python, inifile:
plugins: mock-1.10.0
collected 1 item

tests/unit/test_handler.py::test_lambda_handler PASSED                                                                           [100%]

======================================================= 1 passed in 0.16 seconds =======================================================
```

= build

```bash
sam build -u                                                                (env: sam-python-test)──(2018-12-18 13:46)─┘
2018-12-18 13:46:24 Starting Build inside a container
2018-12-18 13:46:24 Building resource 'HelloWorldFunction'

Fetching lambci/lambda:build-python3.6 Docker container image......
2018-12-18 13:46:26 Mounting /Users/mvanbaak/dev/frontrow/sam-python/hello_world as /tmp/samcli/source:ro inside runtime container

Build Succeeded

Built Artifacts  : .aws-sam/build
Built Template   : .aws-sam/build/template.yaml

Commands you can use next
=========================
[*] Invoke Function: sam local invoke
[*] Package: sam package --s3-bucket <yourbucket>

Running PythonPipBuilder:ResolveDependencies
Running PythonPipBuilder:CopySource
```

= run

```bash
sam local start-api                                                         (env: sam-python-test)──(2018-12-18 13:46)─┘
2018-12-18 13:46:33 Mounting HelloWorldFunction at http://127.0.0.1:3000/hello [GET]
2018-12-18 13:46:33 You can now browse to the above endpoints to invoke your functions. You do not need to restart/reload SAM CLI while working on your functions changes will be reflected instantly/automatically. You only need to restart SAM CLI if you update your AWS SAM template
2018-12-18 13:46:33  * Running on http://127.0.0.1:3000/ (Press CTRL+C to quit)
2018-12-18 13:46:35 Invoking app.lambda_handler (python3.6)

Fetching lambci/lambda:python3.6 Docker container image......
2018-12-18 13:46:37 Mounting /Users/mvanbaak/dev/frontrow/sam-python/.aws-sam/build/HelloWorldFunction as /var/task:ro inside runtime container
START RequestId: 48edee5b-0672-496b-ba99-bd2832f3cee9 Version: $LATEST
Unable to import module 'app': attempted relative import with no known parent package
END RequestId: 48edee5b-0672-496b-ba99-bd2832f3cee9
REPORT RequestId: 48edee5b-0672-496b-ba99-bd2832f3cee9 Duration: 826 ms Billed Duration: 900 ms Memory Size: 128 MB Max Memory Used: 26 MB
2018-12-18 13:46:39 No Content-Type given. Defaulting to 'application/json'.
2018-12-18 13:46:39 127.0.0.1 - - [18/Dec/2018 13:46:39] "GET /hello HTTP/1.1" 200 -
```
