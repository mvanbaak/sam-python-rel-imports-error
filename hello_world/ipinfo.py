import requests

def get_my_ip():
    ip = requests.get("http://checkip.amazonaws.com/")
    return ip.text.replace("\n", " ")
